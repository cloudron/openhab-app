[0.1.0]
* Initial version

[0.2.0]
* Update OpenHAB to 3.4.0

[0.3.0]
* Update OpenHAB to 3.4.1

[1.0.0]
* This marks the first stable app package release

[1.0.1]
* Update OpenHAB to 3.4.2

[1.0.2]
* Update OpenHAB to 3.4.3
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/3.4.3)
* Fix overriding Jetty's User-Agent in HttpUtil
* Fix default human language interpreter selection
* Use commandDescription instead of stateDescription for selection list
* Fix redirection for reverse proxy with authentication not working
* Fix WebAudio sink not playing on Safari

[1.0.3]
* Update OpenHAB to 3.4.4
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/3.4.4)

[1.0.4]
* Update OpenHAB to 3.4.5
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/3.4.5)

[1.1.0]
* Update OpenHAB to 4.0.1
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.0.1)

[1.1.1]
* Update OpenHAB to 4.0.2
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.0.2)

[1.1.2]
* Update OpenHAB to 4.0.3
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.0.3)

[1.2.0]
* Update base image to 4.2.0

[1.2.1]
* Update OpenHAB to 4.0.4
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.0.4)
* FolderObserver improvements
* Ensure semantic model pages are built after tags are loaded
* Fix SSE failure toast container not removed
* Fix decimal formatting for oh-stepper

[1.3.0]
* Update OpenHAB to 4.1.0
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.1.0)

[1.3.1]
* Update OpenHAB to 4.1.1
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.1.1)
* Add network-interface context
* Fix duplicate UIDs in remote add-on services
* Fix month, week, day not supported
* Add no-cache directive to cached REST responses
* Consider network settings to avoid creating unexpected JmDNS instances
* Fix parsing of button (buttongrid element built with MainUI)
* Respond with 404 if add-on is missing in add-on service
* Fix syntax warning for empty rule condition in managed sitemap provider

[1.3.2]
* Update OpenHAB to 4.1.2
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.1.2)

[1.3.3]
* Update OpenHAB to 4.1.3
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.1.3)

[1.4.0]
* Update OpenHAB to 4.2.0
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.2.0)

[1.4.1]
* Update OpenHAB to 4.2.1
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.2.1)
* SSRF/XSS (CometVisu) https://github.com/openhab/openhab-webui/security/advisories/GHSA-v7gr-mqpj-wwh3
* Sensitive information disclosure (CometVisu) https://github.com/openhab/openhab-webui/security/advisories/GHSA-3g4c-hjhr-73rj
* RCE through path traversal (CometVisu) https://github.com/openhab/openhab-webui/security/advisories/GHSA-f729-58x4-gqgf
* Path traversal (CometVisu) https://github.com/openhab/openhab-webui/security/advisories/GHSA-pcwp-26pw-j98w

[1.4.2]
* Update OpenHAB to 4.2.2
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.2.2)

[1.4.3]
* Update openhab-distro to 4.2.3
* [Full Changelog](https://github.com/openhab/openhab-distro/releases/tag/4.2.3)

[1.5.0]
* Update openhab-distro to 4.3.0
* [Full Changelog](https://github.com/openhab/openhab-distro/releases/tag/4.3.0)
* Notes about new [add-ons](#newaddons)
* Notes about the [core runtime](#core-runtime)
* Notes about the [UIs](#uis)
* Notes about the [add-ons](#addons)
* [Upgrade Process](#upgrade-process) for [openHABian](#openhabian), [APT](#package-based-installations), [RPM](#rpm), [Manual Installations](#manual-installations)
* [Breaking Changes](#breaking-changes-that-require-manual-interaction-after-the-upgrade) that require manual interaction after the upgrade.

[1.5.1]
* Update openhab-distro to 4.3.1
* [Full changelog](https://github.com/openhab/openhab-distro/releases/tag/4.3.1)

[1.5.2]
* Update openhab-distro to 4.3.2
* [Full Changelog](https://github.com/openhab/openhab-distro/releases/tag/4.3.2)

[1.5.3]
* Update openhab-distro to 4.3.3
* [Full Changelog](https://github.com/openhab/openhab-distro/releases/tag/4.3.3)
* Fix handling of multiple $DELTA conditions
* Convert to relative unit in State Filter's Delta check
* Fix NullPointerException
* Adjust configuration to new API limits
* Fix setting vacation hold temperatures and ignore unrealistic actual temperature
* Update tariff filter for Netselskabet Elværk
* Update tariff filter for FLOW Elnet

