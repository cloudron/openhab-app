#!/bin/bash

set -eu

echo "==> Ensure directories"
mkdir -p /app/data/userdata/tmp /app/data/addons /app/data/automation

if [[ ! -d "/app/data/userdata/etc" ]]; then
    echo "==> Copy initial userdata/etc"
    cp -rf /app/pkg/userdata-etc /app/data/userdata/etc
fi

# make sure userdata/etc/config.properties has correct values on upgrade
# all custom values has to be kept in userdata/etc/custom.properties
echo "==> Copy config properties"
cp /app/pkg/userdata-etc/config.properties /app/data/userdata/etc/config.properties

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Upgrade DB"
DB_PATH=/app/code/userdata/jsondb
if [[ -d "${DB_PATH}" ]]; then
    dbFiles=("org.openhab.core.items.Item.json" "org.openhab.core.items.Metadata.json" "org.openhab.core.thing.Thing.json" "org.openhab.core.thing.link.ItemChannelLink.json" "org.openhab.core.thing.link.ItemThingLink.json" "uicomponents_ui_page.json" "users.json")
    for i in ${!dbFiles[@]}; do
        # create db file if not exists
        [[ ! -f "${DB_PATH}/${dbFiles[$i]}" ]] && gosu cloudron:cloudron echo '{}' > "${DB_PATH}/${dbFiles[$i]}"
    done
    gosu cloudron:cloudron java -jar /app/code/runtime/bin/upgradetool.jar --dir /app/code/userdata --force
fi

echo "==> Starting OpenHAB"
exec gosu cloudron:cloudron /app/code/runtime/bin/karaf daemon
