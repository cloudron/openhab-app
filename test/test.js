#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;
    const admin_username = 'admin';
    const admin_password = 'changeme';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
        await browser.sleep(5000);
    }

    async function getMainPage() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//div[contains(@class,"title-large-text") and contains(., "Overview")]'));
    }

    async function setupWizard() {
        await browser.findElement(By.xpath('//a[contains(@class,"button") and contains(., "Begin Setup")]')).click();
        await browser.sleep(3000);

        await browser.findElement(By.xpath('//a[contains(@class,"button") and contains(., "Configure in Settings Later")]')).click();
        await browser.sleep(3000);

        await browser.findElement(By.xpath('//a[contains(@class,"button") and contains(., "Install Persistence Add-ons Later")]')).click();
        await browser.sleep(3000);

        await browser.findElement(By.xpath('//a[contains(@class,"button") and contains(., "Install Add-ons Later")]')).click();
        await browser.sleep(3000);

        await browser.findElement(By.xpath('//a[contains(@class,"button") and contains(., "Get Started")]')).click();
        await browser.sleep(3000);

        await waitForElement(By.xpath('//div[contains(@class,"title-large-text") and contains(., "Overview")]'));
    }

    async function initialSetup() {
        await browser.sleep(20000);

        await browser.get(`https://${app.fqdn}`);

        await browser.sleep(2000);
        await waitForElement(By.xpath('//input[@name="username"]'));

        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(admin_username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(admin_password);
        await browser.findElement(By.xpath('//input[@name="password_repeat"]')).sendKeys(admin_password);
        await browser.findElement(By.xpath('//input[@type="Submit" and @value="Create Account"]')).click();

        await waitForElement(By.xpath('//div[contains(text(), "Language")]'));
    }

    async function login() {
        await browser.manage().deleteAllCookies();

        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//a[contains(@class,"button") and contains(.,"lock_shield_fill")]'));
        await browser.findElement(By.xpath('//a[contains(@class,"button") and contains(.,"lock_shield_fill")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(admin_username);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(admin_password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@type="Submit" and @value="Sign In"]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//div[contains(@class,"title-large-text") and contains(., "Overview")]'));
    }

    async function createModel() {
        await browser.get(`https://${app.fqdn}/settings/model/`);
        await browser.sleep(5000);

        await browser.findElement(By.xpath('//a[text()="Add Location"]')).click();
        await browser.sleep(5000);

        await browser.findElement(By.xpath('//input[@placeholder="A unique identifier for the Item."]')).sendKeys('Home');
        await browser.sleep(4000);

        await browser.findElement(By.xpath('//a[contains(@class, "button") and contains(text(), "Create")]')).click();
        await browser.sleep(3000);
    }

    async function checkModel() {
        await browser.get(`https://${app.fqdn}/settings/model/`);
        await browser.wait(until.elementLocated(By.xpath('//div[@class="treeview-item-label"  and contains(text(), "Home")]')), TEST_TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can initial setup', initialSetup);
    it('can setup wizard', setupWizard);

    it('can get main page', getMainPage);

    it('can create Model', createModel);
    it('can check Model', checkModel);

    it('can restart app', async function () {
        await browser.get('about:blank');
        await clearCache();
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('can check Model', checkModel);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        await clearCache();
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can login', login);
    it('can check Model', checkModel);

    it('move to different location', async function () {
        await browser.get('about:blank');
        await clearCache();
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can check Model', checkModel);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', async function () {
        execSync(`cloudron install --appstore-id org.openhab.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can initial setup', initialSetup);
    it('can setup wizard', setupWizard);

    it('can create Model', createModel);
    it('can check Model', checkModel);

    it('can update', async function () {
        await browser.get('about:blank');
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get main page', getMainPage);
    it('can check Model', checkModel);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
