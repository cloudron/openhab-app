FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg/
WORKDIR /app/code

# given the cloudron user (1000) access to potentially added /devices
RUN usermod -a -G dialout cloudron

RUN apt-get update && apt-get install -y openjdk-17-jre-headless && rm -rf /var/cache/apt /var/lib/apt/lists

# The M version means "milestone" . These are just periodic builds
# renovate: datasource=github-releases depName=openhab/openhab-distro versioning=semver extractVersion=^(?<version>\d+\.\d+\.\d+)$
ARG OPENHAB_VERSION=4.3.3

RUN curl -LSs https://github.com/openhab/openhab-distro/releases/download/${OPENHAB_VERSION}/openhab-${OPENHAB_VERSION}.tar.gz | tar -xz -C /app/code/ -f -

RUN mv /app/code/userdata/etc /app/pkg/userdata-etc && \
    ln -sf /app/data/automation /app/code/conf/automation && \
    mv /app/code/addons /app/code/addons.orig && \
    ln -sf /app/data/addons /app/code/addons

RUN rm -rf /app/code/userdata && ln -sf /app/data/userdata /app/code/userdata

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
